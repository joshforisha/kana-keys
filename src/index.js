import Application from 'components/Application';
import React from 'react';
import { render } from 'react-dom';

render(<Application />, document.getElementById('Application'));
