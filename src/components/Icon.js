import PropTypes from 'prop-types';
import React from 'react';
import feather from 'feather-icons';
import styled from 'styled-components';

const Icon_ = styled.span`
  display: inline-block;
  height: 1em;
  width: 1em;

  & > svg {
    height: 1em;
    margin-bottom: -2px;
    width: 1em;
  }
`;

export default function Icon(props) {
  return (
    <Icon_
      className={props.className}
      dangerouslySetInnerHTML={{ __html: feather.icons[props.name].toSvg() }}
    />
  );
}

Icon.propTypes = {
  className: PropTypes.string,
  name: PropTypes.string.isRequired
};
