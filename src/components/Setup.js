import Button from 'components/Button';
import Keyboard from 'components/Keyboard';
import PropTypes from 'prop-types';
import React from 'react';
import Themes from 'lib/Themes';
import styled from 'styled-components';

const Setup_ = styled.div`
  text-align: center;
`;

const ToggleButton = styled(Button)`
  margin: 0 var(--tiny);
`;

const Toggles = styled.div`
  margin-bottom: var(--large);
`;

const containsAll = (a, b) => b.every(c => a.indexOf(c) > -1);

const handleToggle = (props, keys, isEnabled) =>
  keys
    .filter(
      a =>
        isEnabled
          ? props.enabledKeys.indexOf(a) > -1
          : props.enabledKeys.indexOf(a) === -1
    )
    .forEach(a => props.onToggleKey(a));

const bottomRow = ['つ', 'さ', 'そ', 'ひ', 'こ', 'み', 'も', 'ね', 'る', 'め'];

const highRow = [
  'た',
  'て',
  'い',
  'す',
  'か',
  'ん',
  'な',
  'に',
  'ら',
  'せ',
  '゛',
  'む',
  'へ'
];

const homeRow = [
  'ち',
  'と',
  'し',
  'は',
  'き',
  'く',
  'ま',
  'の',
  'り',
  'れ',
  'け'
];

const topRow = [
  'ぬ',
  'ふ',
  'あ',
  'う',
  'え',
  'お',
  'や',
  'ゆ',
  'よ',
  'わ',
  'ほ',
  '゜'
];

export default function Setup(props) {
  const bottomRowEnabled = containsAll(props.enabledKeys, bottomRow);
  const highRowEnabled = containsAll(props.enabledKeys, highRow);
  const homeRowEnabled = containsAll(props.enabledKeys, homeRow);
  const topRowEnabled = containsAll(props.enabledKeys, topRow);

  return (
    <Setup_>
      <Keyboard
        enabledKeys={props.enabledKeys}
        onToggleKey={props.onToggleKey}
      />
      <Toggles>
        <ToggleButton
          icon={topRowEnabled ? 'minus' : 'plus'}
          onClick={() => handleToggle(props, topRow, topRowEnabled)}
          theme={topRowEnabled ? Themes.Cabernet : Themes.BrownSugar}
        >
          Top Row
        </ToggleButton>
        <ToggleButton
          icon={highRowEnabled ? 'minus' : 'plus'}
          onClick={() => handleToggle(props, highRow, highRowEnabled)}
          theme={highRowEnabled ? Themes.Cabernet : Themes.BrownSugar}
        >
          High Row
        </ToggleButton>
        <ToggleButton
          icon={homeRowEnabled ? 'minus' : 'plus'}
          onClick={() => handleToggle(props, homeRow, homeRowEnabled)}
          theme={homeRowEnabled ? Themes.Cabernet : Themes.BrownSugar}
        >
          Home Row
        </ToggleButton>
        <ToggleButton
          icon={bottomRowEnabled ? 'minus' : 'plus'}
          onClick={() => handleToggle(props, bottomRow, bottomRowEnabled)}
          theme={bottomRowEnabled ? Themes.Cabernet : Themes.BrownSugar}
        >
          Bottom Row
        </ToggleButton>
      </Toggles>
      <Button
        disabled={props.enabledKeys.length < 3}
        icon="arrow-right"
        onClick={props.onStart}
        theme={Themes.Midori}
      >
        Start
      </Button>
    </Setup_>
  );
}

Setup.propTypes = {
  enabledKeys: PropTypes.arrayOf(PropTypes.string).isRequired,
  onStart: PropTypes.func.isRequired,
  onToggleKey: PropTypes.func.isRequired
};
