import styled from 'styled-components';

export default styled.span`
  background-color: ${p =>
    p.highlighted ? 'var(--black-beauty)' : 'rgba(49, 48, 53, 0.25)'};
  border: 1px solid ${p => (p.bump ? 'var(--nibi)' : 'transparent')};
  border-radius: var(--tiny);
  color: var(--light);
  cursor: pointer;
  display: inline-block;
  margin: var(--tiny) 0 0 var(--tiny);
  padding: 0 var(--small);
  text-align: center;
`;
