import PropTypes from 'prop-types';
import React from 'react';
import styled from 'styled-components';

const Bar = styled.div`
  background-color: rgba(49, 48, 53, 0.1);
  height: 4px;
  margin: 0 auto;
  width: 200px;
`;

const Progress = styled.div`
  background-color: ${p => p.color};
  height: 4px;
  transition: background-color var(--slow) ease-out;
`;

export default function TimerBar(props) {
  let color;
  if (props.progress < 50) color = 'var(--matsuba)';
  else if (props.progress < 75) color = 'var(--yamabuki)';
  else color = 'var(--akane)';

  return (
    <Bar>
      <Progress color={color} style={{ width: props.progress + '%' }} />
    </Bar>
  );
}

TimerBar.propTypes = {
  progress: PropTypes.number.isRequired
};
