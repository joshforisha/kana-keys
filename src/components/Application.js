import React from 'react';
import Setup from 'components/Setup';
import TimerBar from 'components/TimerBar';
import randomItem from 'lib/random-item';
import styled from 'styled-components';

const Prompt = styled.h1`
  font-size: 4em;
  margin: var(--large) 0 0;
  text-align: center;
`;

const Score = styled.h2`
  color: rgba(49, 48, 53, 0.25);
  font-size: 2em;
  margin: 0;
  text-align: center;
`;

const allowedTime = 6000;

export default class Application extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      enabledKeys: ['ち', 'と', 'し', 'は', 'ま', 'の', 'り', 'れ'],
      endTime: 0,
      keyPresses: 0,
      matches: 0,
      startTime: 0,
      time: 0,
      quizCharacter: null
    };

    this.progressTime = this.progressTime.bind(this);
    this.resetTime = this.resetTime.bind(this);
    this.selectCharacter = this.selectCharacter.bind(this);
    this.start = this.start.bind(this);
    this.toggleKey = this.toggleKey.bind(this);
  }

  componentDidMount() {
    window.addEventListener('keypress', ev => {
      if (this.state.quizCharacter === null) return;

      if (ev.key === this.state.quizCharacter) {
        this.resetTime();
        this.setState(state => ({
          keyPresses: state.keyPresses + 1,
          matches: state.matches + 1
        }));
        this.selectCharacter();
      } else {
        this.setState(state => ({ keyPresses: state.keyPresses + 1 }));
      }
    });
  }

  progressTime() {
    this.setState(() => ({
      time: Date.now()
    }));
    window.requestAnimationFrame(this.progressTime);
  }

  resetTime() {
    const now = Date.now();

    this.setState(() => ({
      endTime: now + allowedTime,
      time: now,
      startTime: now
    }));
  }

  selectCharacter() {
    const possibleKeys = this.state.enabledKeys.filter(
      key => key !== this.state.quizCharacter
    );

    this.setState(() => ({ quizCharacter: randomItem(possibleKeys) }));
  }

  start() {
    this.resetTime();
    this.progressTime();
    this.selectCharacter();
  }

  toggleKey(character) {
    this.setState(state =>
      Object.assign(state, {
        enabledKeys:
          state.enabledKeys.indexOf(character) > -1
            ? state.enabledKeys.filter(k => k !== character)
            : state.enabledKeys.concat(character)
      })
    );
  }

  render() {
    if (this.state.quizCharacter !== null) {
      const progress = Math.min(
        100,
        100 * (this.state.time - this.state.startTime) / allowedTime
      );

      return [
        <Prompt key="prompt">{this.state.quizCharacter}</Prompt>,
        <TimerBar key="timer-bar" progress={progress} />,
        <Score key="score">
          {Math.round(100 * this.state.matches / (this.state.keyPresses || 1))}%
        </Score>
      ];
    }

    return (
      <Setup
        enabledKeys={this.state.enabledKeys}
        onStart={this.start}
        onToggleKey={this.toggleKey}
      />
    );
  }
}
