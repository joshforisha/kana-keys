import Sizes from 'lib/Sizes';
import styled from 'styled-components';

const widths = {
  [Sizes.Huge]: '4rem',
  [Sizes.Large]: '2rem',
  [Sizes.Medium]: '1rem',
  [Sizes.Small]: '0.5rem',
  [Sizes.Tiny]: '0.25rem'
};

export default styled.div`
  display: inline-block;
  width: ${p => widths[p.size]};
`;
