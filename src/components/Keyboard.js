import Key from 'components/Key';
import PropTypes from 'prop-types';
import React from 'react';
import Sizes from 'lib/Sizes';
import Space from 'components/Space';
import styled from 'styled-components';

const Keyboard_ = styled.div`
  margin: var(--large) auto;
  padding: 0 var(--tiny) var(--tiny) 0;
  text-align: left;
  width: 540px;
`;

const Row = styled.div``;

export default function Keyboard({ className, enabledKeys, onToggleKey }) {
  const charKey = (character, bump = false) => (
    <Key
      bump={bump}
      highlighted={enabledKeys.indexOf(character) > -1}
      onClick={() => onToggleKey(character)}
    >
      {character}
    </Key>
  );

  return (
    <Keyboard_ className={className}>
      <Row>
        {charKey('ぬ')}
        {charKey('ふ')}
        {charKey('あ')}
        {charKey('う')}
        {charKey('え')}
        {charKey('お')}
        {charKey('や')}
        {charKey('ゆ')}
        {charKey('よ')}
        {charKey('わ')}
        {charKey('ほ')}
        {charKey('゜')}
      </Row>
      <Row>
        <Space size={Sizes.Medium} />
        {charKey('た')}
        {charKey('て')}
        {charKey('い')}
        {charKey('す')}
        {charKey('か')}
        {charKey('ん')}
        {charKey('な')}
        {charKey('に')}
        {charKey('ら')}
        {charKey('せ')}
        {charKey('゛')}
        {charKey('む')}
        {charKey('へ')}
      </Row>
      <Row>
        <Space size={Sizes.Medium} />
        <Space size={Sizes.Small} />
        {charKey('ち')}
        {charKey('と')}
        {charKey('し')}
        {charKey('は', true)}
        {charKey('き')}
        {charKey('く')}
        {charKey('ま', true)}
        {charKey('の')}
        {charKey('り')}
        {charKey('れ')}
        {charKey('け')}
      </Row>
      <Row>
        <Space size={Sizes.Large} />
        <Space size={Sizes.Small} />
        {charKey('つ')}
        {charKey('さ')}
        {charKey('そ')}
        {charKey('ひ')}
        {charKey('こ')}
        {charKey('み')}
        {charKey('も')}
        {charKey('ね')}
        {charKey('る')}
        {charKey('め')}
      </Row>
    </Keyboard_>
  );
}

Keyboard.propTypes = {
  className: PropTypes.string,
  enabledKeys: PropTypes.arrayOf(PropTypes.string).isRequired,
  onToggleKey: PropTypes.func.isRequired
};
