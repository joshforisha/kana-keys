import Icon from 'components/Icon';
import PropTypes from 'prop-types';
import React from 'react';
import Themes from 'lib/Themes';
import styled from 'styled-components';
import { Theme } from 'lib/types';

const Button_ = styled.button`
  background-color: ${p => p.theme.base};
  border-radius: var(--tiny);
  border-style: none;
  color: ${p => p.theme.contrast};
  cursor: ${p => (p.disabled ? 'not-allowed' : p.clickable ? 'pointer' : null)};
  font-size: 1em;
  outline: none;
  padding: var(--small) var(--medium);
  transition: background-color var(--slow) ease-out;
`;

const Icon_ = styled(Icon)`
  margin-right: ${p => (p.spaced ? 'var(--small)' : null)};
  opacity: 0.75;
`;

export default function Button(props) {
  const clickable = props.hasOwnProperty('onClick');
  const spaced = props.hasOwnProperty('children');
  const icon = props.hasOwnProperty('icon') ? (
    <Icon_ name={props.icon} spaced={spaced} />
  ) : null;

  return (
    <Button_
      className={props.className}
      clickable={clickable}
      disabled={props.disabled}
      onClick={props.onClick}
      theme={props.theme}
    >
      {icon}
      {props.children}
    </Button_>
  );
}

Button.defaultProps = {
  disabled: false,
  theme: Themes.BrownSugar
};

Button.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  disabled: PropTypes.bool,
  icon: PropTypes.string,
  onClick: PropTypes.func,
  theme: Theme
};
