const Sizes = {
  Huge: 'huge',
  Large: 'large',
  Medium: 'medium',
  Small: 'small',
  Tiny: 'tiny'
};

export default Sizes;
