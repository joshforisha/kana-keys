import PropTypes from 'prop-types';

export const Theme = PropTypes.shape({
  base: PropTypes.string.isRequired,
  contrast: PropTypes.string.isRequired
});
