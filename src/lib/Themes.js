const Themes = {
  BlackBeauty: {
    base: 'var(--black-beauty)',
    contrast: 'var(--light)'
  },
  BrownSugar: {
    base: 'var(--brown-sugar)',
    contrast: 'var(--light)'
  },
  Cabernet: {
    base: 'var(--cabernet)',
    contrast: 'var(--light)'
  },
  CoconutMilk: {
    base: 'var(--coconut-milk)',
    contrast: 'var(--dark)'
  }
};

export default Themes;
