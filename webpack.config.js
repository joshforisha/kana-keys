const CompressionPlugin = require('compression-webpack-plugin');
const webpack = require('webpack');

const isProduction = process.env.NODE_ENV === 'production';

const babelPresets = ['env', 'react'];
if (!isProduction) {
  babelPresets.push('react-hmre');
}

const plugins = isProduction
  ? [
      new CompressionPlugin({
        algorithm: 'gzip',
        asset: '[path].gz[query]',
        minRatio: 0.8,
        test: /\.(html|js)$/,
        threshold: 10240
      }),
      new webpack.optimize.UglifyJsPlugin()
    ]
  : [];

module.exports = {
  devtool: 'cheap-module-source-map',
  entry: './src/index.js',
  module: {
    rules: [
      {
        include: `${__dirname}/src/`,
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: babelPresets
          }
        }
      }
    ]
  },
  output: {
    filename: 'bundle.js',
    path: `${__dirname}/public/`
  },
  plugins,
  resolve: {
    modules: ['node_modules/', 'src/']
  }
};
